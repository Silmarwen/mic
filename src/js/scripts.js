$(document).ready(function () {
    var homeSlider = $('.js-home-slider'),
        partnersSlider = $('.js-partners-slider');

    homeSlider.bxSlider({
        auto: true,
        controls: false,
        pager: true
    });

    $('.js-building__img-slider').bxSlider({
        controls: false,
        pagerCustom: '.js-building__img-thumbs'
    });

    $('.js-building__img-thumbs').bxSlider({
        slideWidth: 150,
        auto: false,
        controls: true,
        pager: false,
        minSlides: 4,
        maxSlides: 4,
        infiniteLoop: false,
        slideMargin: 18
    });

    partnersSlider.bxSlider({
        slideWidth: 200,
        minSlides: 6,
        maxSlides: 6,
        moveSlides: 1,
        auto: true,
        controls: true,
        pager: false
    });

    $('.js-attention-close').on('click', function () {
        $('.attention').addClass('has-hide').slideUp(900);
        setTimeout(function () {
            $('.attention').removeClass('has-hide');
        }, 1000);
    });

    $('.js-fancyapps').fancybox({

    });

    //$('.js-range-select').each(function(){
    //    var selfMin = $(this).data('select-min'),
    //        selfMax = $(this).data('select-max');
    //    $(this).slider({
    //        max: selfMax,
    //        range: true,
    //        values: [selfMin, selfMax]
    //    }).slider("pips", {
    //        rest: false,
    //        labels: [selfMin, selfMax]
    //    });
    //});
});

