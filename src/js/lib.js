/**
 * Created by Silmarwen on 23.07.2016.
 */

//= jquery.bxslider.min.js
//= fancyapps/jquery.fancybox.pack.js

(function ($) {
    $(function () {
        $('.js-tabs .tabs__captions').on('click', 'li:not(.is-active)', function () {
            $(this)
                .addClass('is-active').siblings().removeClass('is-active')
                .closest('.js-tabs').find('.tabs__item').removeClass('is-active').eq($(this).index()).addClass('is-active');
        });

    });
})(jQuery);
